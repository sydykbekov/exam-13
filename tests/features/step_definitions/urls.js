const baseUrl = 'http://localhost:3000';

module.exports = {
  main: baseUrl,
  loginUrl: baseUrl+"/login",
  addNewPlace: baseUrl+"/add-new-place"
};