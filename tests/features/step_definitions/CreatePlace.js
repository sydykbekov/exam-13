const urls = require('./urls');
const resolve = require('path').resolve;
let rate;

module.exports = function() {
  this.Given(/^я захожу на страницу логина/, function () {
    return browser.url(urls.loginUrl);
  });

  this.When(/^я ввожу в поле "([^"]*)" значение "([^"]*)"$/, function (fieldName, value) {
    const input = browser.element(`input[name="${fieldName}"]`);
    return input.setValue(value);
  });

  this.When(/^нажимаю на кнопку "([^"]*)"$/, function (text) {
    const button = browser.element(`button=${text}`);
    return button.click();
  });

  this.When(/^я вижу сообщение об успешном логине/, function () {
    const notification = browser.element('.notification-message .title');
    notification.waitForExist(5000);

    const notificationText = browser.element('.notification-message .title').getText();

    return expect(notificationText).toBe('User and password correct!');
  });

  this.When(/^перехожу на страницу добавления заведения/, function () {
    return browser.url(urls.addNewPlace);
  });

  this.When(/^ввожу в поле "([^"]*)" значение "([^"]*)"$/, function (arg1, arg2) {
    const textarea = browser.element(`textarea[name="${arg1}"]`);
    return textarea.setValue(arg2);
  });

  this.When(/^выбираю картинку пользователя/, function () {
    let file = resolve('imageForTests/Paris.jpg');
    return browser.chooseFile("input[type=file]", file);
  });

  this.When(/^принимаю условие создания$/, function () {
    let file = browser.element(`input[type=checkbox]`);
    return file.click();
  });

  this.When(/^я вижу сообщение об успешном создании задачи$/, function () {
    const notification = browser.element('.notification-message .title');
    notification.waitForExist(5000);

    const notificationText = browser.element('.notification-message .title').getText();

    return expect(notificationText).toBe('Success!');
  });

  this.Then(/^в спике тоже вижу созданную задачу$/, function () {
    const div = browser.element('h3=Adriano');
    div.waitForExist(5000);

    return true;
  });

  this.Given(/^я захожу на страницу просмотра заведения$/, function () {
    const div = browser.element('h3=Империя Пиццы');
    div.waitForExist(5000);

    return div.click();
  });

  this.When(/^я выбираю оценку для "([^"]*)" значение "([^"]*)"$/, function (arg1, arg2) {
    const select = browser.element(`select[name=${arg1}] `);
    select.click();
    return select.selectByValue(arg2);
  });

  this.When(/^вижу что рейтинг имеет текст "([^"]*)"$/, function (arg1) {
    browser.pause(2000);
    const rate = browser.element(`span#for-test`);
    rate.waitForExist(5000);

    const rating = rate.getText();
    return expect(rating).toBe(arg1);
  });
};