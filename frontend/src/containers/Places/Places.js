import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Grid, PageHeader, Row} from "react-bootstrap";
import {fetchPlaces, removePlace} from "../../store/actions/places";
import PlaceListItem from "../../components/PlaceListItem/PlaceListItem";

class Places extends Component {
    componentDidMount() {
        this.props.fetchPlaces();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    All places
                </PageHeader>
                <Grid>
                    <Row>
                        {(this.props.places || []).map(item => (
                            <PlaceListItem
                                key={item.place._id}
                                id={item.place._id}
                                title={item.place.title}
                                image={item.place.image}
                                images={item.images}
                                reviews={item.reviews}
                                removePlace={this.props.removePlace}
                                isAdmin={this.props.user && this.props.user.role === 'admin'}
                            />
                        ))}
                    </Row>
                </Grid>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    places: state.places.places
});

const mapDispatchToProps = dispatch => ({
  fetchPlaces: () => dispatch(fetchPlaces()),
  removePlace: id => dispatch(removePlace(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Places);