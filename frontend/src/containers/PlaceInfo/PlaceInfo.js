import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {addImage, addReview, fetchPlaceInfo, removeImage, removeReview} from "../../store/actions/places";
import {Button, Media, PageHeader} from "react-bootstrap";
import config from "../../config";

class PlaceInfo extends Component {
  state = {
    text: '',
    image: '',
    qualityOfFood: '',
    qualityOfService: '',
    interior: ''
  };

  componentDidMount() {
    this.props.fetchPlaceInfo(this.props.match.params.id);
  }

  inputChangeHandler = e => {
    this.setState({[e.target.name]: e.target.value});
  };

  submitReviewFormHandler = e => {
    e.preventDefault();

    const data = {...this.state};
    data.place = this.props.match.params.id;
    data.dateTime = new Date().toISOString();
    delete data.image;
    this.props.addReview(data);
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  submitPhotoHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    formData.append('image', this.state.image);
    formData.append('place', this.props.match.params.id);

    this.props.addImage(formData, this.props.match.params.id);
  };

  render() {
    const place = this.props.place;
    let placeRate = 0;
    let qualityOfFood = 0;
    let qualityOfService = 0;
    let interior = 0;

    if (place.place) {
      place.reviews.forEach(review => {
        qualityOfService += review.qualityOfService;
        qualityOfFood += review.qualityOfFood;
        interior += review.interior;
        placeRate = placeRate + ((review.qualityOfFood + review.qualityOfService + review.interior) / 3);
      });

      placeRate = (placeRate / place.reviews.length).toFixed(1);
      qualityOfService = (qualityOfService / place.reviews.length).toFixed(1);
      qualityOfFood = (qualityOfFood / place.reviews.length).toFixed(1);
      interior = (interior / place.reviews.length).toFixed(1);
    }

    return (
      place.place ? <Fragment>
        <PageHeader>
          {place.place.title}
        </PageHeader>
        <Media>
          <Media.Body>
            <p>
              {place.place.description}
            </p>
          </Media.Body>
          <Media.Right>
            <img width={300} height={200} src={config.apiUrl + 'uploads/' + place.place.image} alt="thumbnail"/>
          </Media.Right>
        </Media>
        <h3>Gallery</h3>
        <div style={{display: 'flex', flexWrap: 'wrap'}}>
          {place.images.map(image =>
            <div key={image._id} style={{width: '150px', height: '150px', position: 'relative',  marginRight: '20px'}}>
              <img width={150} height={150} src={config.apiUrl + 'uploads/' + image.image}/>
              {this.props.user && this.props.user.role === 'admin' && <span style={{
                position: 'absolute',
                top: '0',
                right: '0',
                width: '25px',
                height: '25px',
                background: 'rgba(0, 0, 0, 0.84)',
                color: '#fff',
                textAlign: 'center',
                cursor: 'pointer',
                lineHeight: '25px'
              }}
                    onClick={() => this.props.removeImage(image._id, this.props.match.params.id)}><b>x</b></span>}
            </div>
          )}
        </div>
        <hr/>
        <div>
          <h3>Ratings</h3>
          <div style={{fontWeight: 'bold'}}>Overall - <span id="for-test">{placeRate}</span></div>
          <div>Quality of food - {qualityOfFood}</div>
          <div>Quality of service - {qualityOfService}</div>
          <div>Interior - {interior}</div>
        </div>
        <hr/>
        <div>
          <h3>Reviews</h3>
          {place.reviews.map(review =>
            <div key={review._id}
                 style={{border: '2px dashed #afafaf', padding: '10px', marginBottom: '20px', overflow: 'hidden'}}>
              <div>On {review.dateTime} <b>{review.user.username}</b> said:</div>
              <div style={{padding: '10px', fontStyle: 'italic'}}>{review.text}</div>
              <div><b>Quality of food - {review.qualityOfFood}</b></div>
              <div><b>Quality of service - {review.qualityOfService}</b></div>
              <div><b>Quality of interior - {review.interior}</b></div>
              {this.props.user && this.props.user.role === 'admin' && <Button style={{float: 'right'}} bsStyle="danger"
                      onClick={() => this.props.removeReview(review._id, this.props.match.params.id)}>Remove
                review</Button>}
            </div>
          )}
          {this.props.user && <form onSubmit={this.submitReviewFormHandler}>
            <h3>Add review</h3>
            <textarea required rows={5} style={{width: '100%'}} name="text" onChange={this.inputChangeHandler}/>
            <div style={{display: 'flex', justifyContent: 'space-between'}}>
              <div><b style={{marginRight: '15px'}}>Quality of food</b>
                <select name="qualityOfFood" required onChange={this.inputChangeHandler}
                        value={this.state.qualityOfFood}>
                  <option defaultValue=""/>
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
                </select>
              </div>
              <div><b style={{marginRight: '15px'}}>Quality of service</b>
                <select name="qualityOfService" required onChange={this.inputChangeHandler}
                        value={this.state.qualityOfService}>
                  <option defaultValue=""/>
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
                </select>
              </div>
              <div><b style={{marginRight: '15px'}}>Interior</b>
                <select name="interior" required onChange={this.inputChangeHandler} value={this.state.interior}>
                  <option defaultValue=""/>
                  <option value="5">5</option>
                  <option value="4">4</option>
                  <option value="3">3</option>
                  <option value="2">2</option>
                  <option value="1">1</option>
                </select>
              </div>
              <div>
                <Button bsStyle="primary" type="submit">Submit review</Button>
              </div>
            </div>
          </form>}
        </div>
        <hr/>
        {this.props.user && <form onSubmit={this.submitPhotoHandler}>
          <h3>Upload new photo</h3>
          <input type="file" name="image" onChange={this.fileChangeHandler} required style={{marginBottom: '20px'}}/>
          <Button bsStyle="primary" type="submit">Upload</Button>
        </form>}
        <hr/>
      </Fragment> : null
    )
  }
}

const mapStateToProps = state => ({
  place: state.places.place,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  fetchPlaceInfo: id => dispatch(fetchPlaceInfo(id)),
  addReview: data => dispatch(addReview(data)),
  addImage: (data, id) => dispatch(addImage(data, id)),
  removeReview: (id, placeId) => dispatch(removeReview(id, placeId)),
  removeImage: (id, placeId) => dispatch(removeImage(id, placeId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PlaceInfo)