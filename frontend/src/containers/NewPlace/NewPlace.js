import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import PlaceForm from "../../components/PlaceForm/PlaceForm";
import {addPlace} from "../../store/actions/places";

class NewPlace extends Component {
    render() {
        return (
            <Fragment>
                <PageHeader>New place</PageHeader>
                <PlaceForm user={this.props.user._id} onSubmit={(formData) => this.props.addPlace(formData)} />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  addPlace: formData => dispatch(addPlace(formData))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPlace);