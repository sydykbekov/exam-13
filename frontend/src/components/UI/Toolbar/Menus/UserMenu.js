import React from 'react';
import {Nav, NavItem} from "react-bootstrap";

const UserMenu = ({user, logout}) => {
  const navTitle = (
    <NavItem>
      Hello, <b>{user.username}</b>!
    </NavItem>
  );

  return (
    <Nav pullRight>
      {user && navTitle}
      <NavItem onClick={logout}>Logout</NavItem>
    </Nav>
  )
};

export default UserMenu;