import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";

const Toolbar = ({user, logout}) => (
  <Navbar>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to="/" exact>
          <NavItem>Cafe crititc</NavItem>
        </LinkContainer>
        {user && <LinkContainer to="/add-new-place" exact>
          <NavItem>Add new place</NavItem>
        </LinkContainer>}
      </Nav>

      {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
    </Navbar.Collapse>
  </Navbar>
);

export default Toolbar;