import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";
import './PlaceForm.css';

class ProductForm extends Component {
  state = {
    title: '',
    description: '',
    image: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>

        <FormElement
          propertyName="title"
          title="Title"
          type="text"
          value={this.state.title}
          changeHandler={this.inputChangeHandler}
          required
        />

        <div className="flex-div">
          <label htmlFor="description" style={{width: '15%', textAlign: 'right'}}>Description</label>
          <textarea
            name="description"
            value={this.state.description}
            onChange={this.inputChangeHandler}
            required
            id="description"
            style={{width: '83%', marginBottom: '20px'}}
            rows={10}
          />
        </div>

        <FormElement
          propertyName="image"
          title="Image"
          type="file"
          changeHandler={this.fileChangeHandler}
          required
        />

        <div className="flex-div">
          <div style={{width: '25%'}}>By submitting this from, you agree that the following information will be submitted to the public domain.</div>
          <div style={{width: '70%'}}><label><input type="checkbox" required/> I understand</label></div>
        </div>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Submit new place</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default ProductForm;
