import React from 'react';
import {Button, Col, Thumbnail} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not-found.png';

const PlaceListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    const imagesQuantity = props.images.length;

    let placeRate = 0;

    props.reviews.forEach(review => {
      placeRate = placeRate + ((review.qualityOfFood + review.qualityOfService + review.interior) / 3);
    });

    placeRate = placeRate / props.reviews.length;

    return (
        <Col xs={6} md={3}>
          <Link to={`/${props.id}`}>
            <Thumbnail src={image} style={{cursor: 'pointer'}}/>
            <h3>{props.title}</h3>
            <div>{imagesQuantity} photos</div>
            <div>Rate: {placeRate ? placeRate.toFixed(1) : 0} Reviews: {props.reviews.length}</div>
          </Link>
          {props.isAdmin && <Button bsStyle="danger" onClick={() => props.removePlace(props.id)}>Delete</Button>}
        </Col>
    );
};

PlaceListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default PlaceListItem;