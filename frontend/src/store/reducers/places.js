import {FETCH_PLACES_SUCCESS, FETCH_PLACE_SUCCESS} from "../actions/actionTypes";

const initialState = {
  places: [],
  place: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PLACES_SUCCESS:
      return {...state, places: action.places};
    case FETCH_PLACE_SUCCESS:
      return {...state, place: action.place};
    default:
      return state;
  }
};

export default reducer;