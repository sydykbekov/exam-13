import axios from '../../axios-api';
import {FETCH_PLACES_SUCCESS, FETCH_PLACE_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";
import {NotificationManager} from 'react-notifications';

const fetchPlacesSuccess = places => {
    return {type: FETCH_PLACES_SUCCESS, places};
};

const fetchPlaceSuccess = place => {
  return {type: FETCH_PLACE_SUCCESS, place};
};

export const fetchPlaces = () => {
    return dispatch => {
        axios.get('/places').then(response => {
            dispatch(fetchPlacesSuccess(response.data));
        });
    }
};

export const fetchPlaceInfo = id => {
    return dispatch => {
        axios.get(`/places/${id}`).then(response => {
            dispatch(fetchPlaceSuccess(response.data));
        })
    }
};

export const addPlace = formData => {
    console.log(formData);
    return dispatch => {
        axios.post('places', formData).then(() => {
            NotificationManager.success('Place added!', 'Success!');
            dispatch(push('/'));
        })
    }
};

export const removePlace = id => {
    return dispatch => {
        axios.delete(`places/${id}`).then((response) => {
            dispatch(fetchPlaces());
        })
    };
};

export const addReview = data => {
    return dispatch => {
        axios.post('/reviews', data).then(() => {
            dispatch(fetchPlaceInfo(data.place));
        })
    }
};

export const addImage = (data, id) => {
    return dispatch => {
        axios.post('/images', data).then(() => {
            dispatch(fetchPlaceInfo(id));
        })
    }
};

export const removeReview = (id, placeId) => {
    return dispatch => {
        axios.delete(`/reviews/${id}`).then(response => {
          dispatch(fetchPlaceInfo(placeId))
        })
    }
};

export const removeImage = (id, placeId) => {
  return dispatch => {
    axios.delete(`/images/${id}`).then(response => {
      dispatch(fetchPlaceInfo(placeId))
    })
  }
};