import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import NewPlace from "./containers/NewPlace/NewPlace";
import Register from "./containers/Register/Register";
import Places from "./containers/Places/Places";
import Login from "./containers/Login/Login";
import PlaceInfo from "./containers/PlaceInfo/PlaceInfo";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = ({user}) => (
  <Switch>
    <Route path="/" exact component={Places}/>
    <ProtectedRoute
      isAllowed={user}
      path="/add-new-place"
      exact
      component={NewPlace}
    />
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <Route path="/:id" exact component={PlaceInfo} />
  </Switch>
);

export default Routes;