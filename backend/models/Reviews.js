const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewsSchema = new Schema({
  place: {
    type: Schema.Types.ObjectId,
    ref: 'Places',
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  dateTime: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  qualityOfFood: {
    type: Number,
    required: true
  },
  qualityOfService: {
    type: Number,
    required: true
  },
  interior: {
    type: Number,
    required: true
  }
});

const Reviews = mongoose.model('Reviews', ReviewsSchema);

module.exports = Reviews;