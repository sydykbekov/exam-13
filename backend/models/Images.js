const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImagesSchema = new Schema({
  place: {
    type: Schema.Types.ObjectId,
    ref: 'Places',
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  image: {
    type: String,
    required: true
  }
});

const Images = mongoose.model('Images', ImagesSchema);

module.exports = Images;