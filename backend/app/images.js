const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Images = require('../models/Images');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = () => {
  router.post('/', [auth, upload.single('image')], (req, res) => {
    const data = req.body;
    data.user = req.user._id;
    data.image = req.file.filename;

    const image = new Images(data);
    image.save().then(result => {
      res.send(result);
    });
  });

  router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Images.deleteOne({_id: req.params.id}).then(response => {
      res.send(response);
    })
  });

  return router;
};

module.exports = createRouter;