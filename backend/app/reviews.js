const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Reviews = require('../models/Reviews');

const createRouter = () => {
  router.post('/', auth, (req, res) => {
    const data = req.body;
    data.user = req.user._id;

    const review = new Reviews(data);
    review.save().then(response => {
      res.send(response);
    })
  });

  router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Reviews.deleteOne({_id: req.params.id}).then(response => {
      res.send(response);
    })
  });

  return router;
};

module.exports = createRouter;