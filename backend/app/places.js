const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Places = require('../models/Places');
const Images = require('../models/Images');
const Reviews = require('../models/Reviews');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = () => {
  router.get('/', (req, res) => {
    Places.find().populate('user').then(result => {
      let array = [];
      result.forEach(place => {
        array.push(new Promise(async (resolve, reject) => {
          let placeImages = await Images.find({place: place._id});
          let placeReviews = await Reviews.find({place: place._id});
          resolve({
            place: place,
            reviews: placeReviews,
            images: placeImages
          });
        }));
      });
      Promise.all(array).then(result => res.send(result));
    })
  });

  router.get('/:id', async (req, res) => {
    const place = await Places.findById({_id: req.params.id});
    let images = await Images.find({place: place._id});
    let reviews = await Reviews.find({place: place._id}).populate('user');
    await res.send({place, images, reviews});
  });

  router.post('/', [auth, upload.single('image')], (req, res) => {
    const data = req.body;
    data.user = req.user._id;
    data.image = req.file.filename;

    const place = new Places(data);
    place.save().then(result => {
      res.send(result);
    });
  });

  router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Places.deleteOne({_id: req.params.id}).then(response => {
      res.send(response);
    })
  });

  return router;
};

module.exports = createRouter;