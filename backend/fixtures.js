const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Places = require('./models/Places');
const Images = require('./models/Images');
const Reviews = require('./models/Reviews');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
    await db.dropCollection('places');
    await db.dropCollection('images');
    await db.dropCollection('reviews');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [Alex, John] = await User.create({
    username: 'Alex',
    password: '123',
    role: 'admin'
  }, {
    username: 'John',
    password: '123',
    role: 'user'
  });

  const [place1, place2, place3, place4] = await Places.create({
    title: 'Империя Пиццы',
    description: 'Пицерия',
    image: 'Cat.jpg',
    user: Alex._id
  }, {
    title: 'Aurora',
    description: 'Ресторан',
    image: 'Dog.jpg',
    user: Alex._id
  }, {
    title: 'Центр плова',
    description: 'Кафе',
    image: 'Mountain.jpg',
    user: John._id
  }, {
    title: 'KFC',
    description: 'Fast Food',
    image: 'Paris.jpg',
    user: Alex._id
  });

  await Images.create({
    place: place1._id,
    user: Alex._id,
    image: 'Cat.jpg'
  }, {
    place: place1._id,
    user: Alex._id,
    image: 'Dog.jpg'
  }, {
    place: place1._id,
    user: Alex._id,
    image: 'Mountain.jpg'
  }, {
    place: place2._id,
    user: Alex._id,
    image: 'Paris.jpg'
  }, {
    place: place4._id,
    user: John._id,
    image: 'Cat.jpg'
  });

  await Reviews.create({
    place: place1._id,
    user: Alex._id,
    dateTime: new Date().toISOString(),
    text: 'Hello, world!',
    qualityOfFood: 5,
    qualityOfService: 4,
    interior: 5
  }, {
    place: place1._id,
    user: John._id,
    dateTime: new Date().toISOString(),
    text: 'Hello, world!',
    qualityOfFood: 5,
    qualityOfService: 3,
    interior: 2
  }, {
    place: place1._id,
    user: Alex._id,
    dateTime: new Date().toISOString(),
    text: 'Hello, world!',
    qualityOfFood: 4,
    qualityOfService: 4,
    interior: 5
  }, {
    place: place2._id,
    user: John._id,
    dateTime: new Date().toISOString(),
    text: 'Hello, world!',
    qualityOfFood: 3,
    qualityOfService: 4,
    interior: 5
  });

  db.close();
});